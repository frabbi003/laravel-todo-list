<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Tasks;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\CreateTaskRequest;
class TodoController extends Controller
{
    public function index(){
    	$data = Tasks::all();//all() = call all data from database
    	return view('home')->with('add',$data);// 'home' = home.blade.php
    }

    public function store(CreateTaskRequest $request){ //method injection
    	$create = [

    			'name'=>Input::get('name'),
    			'status'=>'Incomplete'
				
    	];

    	$response = Tasks::create($create);//[Method_name::create(variable_name)] for creating
    	if($response){
    	return redirect()->back()->with('success','Task created successfully');
    	}
    }

    public function edit($id){
    	 $data = Tasks::find($id);
    	 return view('todo-edit')->with('data',$data);
    }

    public function update(CreateTaskRequest $request, $id){
    	$data = [

    			'name'=>Input::get('name'),
    			'status'=>Input::get('status')
				
    	];

    	$response = Tasks::find($id)->update($data);//[Method_name::create(variable_name)] for creating
    	if($response){
    	return redirect('/')->with('success','Task updated successfully');
    	}
    }

    public function destroy($id){
    	$response = Tasks::find($id)->delete();
    	if($response)
    	{
    	return redirect('/')->with('success','Task deleted successfully');
    	}
    }


}
