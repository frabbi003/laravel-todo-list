<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Task List</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
  <div class="page-header">
    <h1>Todo Application</h1>
  </div>
</div>
	
	@if (count($errors) >0)
					<div class="alert alert-danger">
						<strong>Whoops!!!</strong> There were some problem with your input.<br><br>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{$error}}</li>
							@endforeach
						</ul>
					</div>
	@endif

	@if(Session::has('success'))
		<div class="alert alert-success">
			{{Session::get('success')}}
		</div>
	@endif

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>Task List</h3>
				<form action="" class="form-inline" method="POST" style="margin-bottom:10px;">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="text" class="form-control" name="name" placeholder="Enter the task name">
					<input type="submit" class="btn btn-primary" value="Save">
				</form>

				<table class="table">
					<tr>
						<td>SI</td>
						<td>Task Name</td>
						<td>Status</td>
						<td>Action</td>
					</tr>
					<?php $i=1; ?>
					@foreach ($add as $row)
					<tr>
						<td>{{$i}}</td>
						<td>{{$row->name}}</td>
						<td>{{$row->status}}</td>
						<td>
							<a href="{{route('getEditRoute',$row->id)}}" class="btn btn-warning">Update</a>
							<form action="{{route('DeleteTask',$row->id)}}"" method="post" style="display:inline;" onsubmit="if (confirm('Delete? Are you sure?')) {return true} else {return false};">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<button type="submit" class="btn btn-danger">Delete</button>
							</form>
							
						</td>
					</tr>
					<?php $i=$i+1; ?>
					@endforeach
				</table>
			</div>
		</div>
	</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>