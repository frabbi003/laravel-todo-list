<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Task List</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
  <div class="page-header">
    <h1>Todo Update</h1>
  </div>
</div>
	
	@if (count($errors) >0)
					<div class="alert alert-danger">
						<strong>Whoops!!!</strong> There were some problem with your input.<br><br>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{$error}}</li>
							@endforeach
						</ul>
					</div>
					@endif

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>Task List</h3>
				<form action="{{route('postEditRoute',$data->id)}}" class="form-inline" method="POST" style="margin-bottom:10px;">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="text" class="form-control" name="name" value="{{$data->name}}">
					<input type="text" class="form-control" name="status" value="{{$data->status}}">
					<input type="submit" class="btn btn-success" value="Update">
				</form>
			</div>
		</div>
	</div>

</body>
</html>